//
//  APIService.swift
//  DemoSPMApplication
//
//  Created by Ly Boung on 21/8/23.
//

import UIKit
import APIRequestManagerSPM

extension RequestProtocol {
  var baseURL: String { "https://hub.dummyapis.com" }
}

enum APIRequest: RequestProtocol {
  case get
  case post
  
  var endPoint: String { "/delay" }
  
  var httpHeader: APIRequestManagerSPM.HTTPHeader? { nil }
  
  var httpMethod: APIRequestManagerSPM.HTTPMethod { .get }
  
  var paramater: APIRequestManagerSPM.Paramater? { ["seconds": 5] }
  
  var encoding: APIRequestManagerSPM.EncodingMethod { .urlEncoding }
  
  var requiredAuthentication: Bool { false }
}

class APIService {
  static func requestAPI() {
    let request: APIRequest = .get
    
    APIRequestManager.shared.fetch(String.self, request: request, preferredLoadingView: true) { response in
      // MARK: - Handle your success response here
      print("Request Success")
    } onFailure: { error in
      // MARK: - Handle your error here
      print("Request Error")
    }
  }
}
