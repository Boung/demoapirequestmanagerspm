//
//  MultipartService.swift
//  DemoSPMApplication
//
//  Created by Ly Boung on 21/8/23.
//

import UIKit
import APIRequestManagerSPM

class MultipartRequest: MultipartRequestProtocol {
  
  var baseURL: String { "YOUR BASE URL" }
  
  var endPoint: String { "YOUR ENDPOINT" }
  
  var httpHeader: APIRequestManagerSPM.HTTPHeader { ["Authorization": "YOUR TOKEN"] }
  
  var httpMethod: APIRequestManagerSPM.HTTPMethod { .post }
  
  var paramater: APIRequestManagerSPM.Paramater? { nil }
  
  var encoding: APIRequestManagerSPM.EncodingMethod { .jsonEncoding }
  
  var requiredAuthentication: Bool { false }
  
  var boundary: String { "YOUR BOUNDARY" }
  
  var fileDatas: [APIRequestManagerSPM.MultipartParamater] {
    return [
      .init(file: UIImage(systemName: "square.and.arrow.up.fill")!.pngData()!, fileName: "icon", name: "file", mimeType: .imagePNG),
    ]
  }
}

class MultipartService {
  static func upload() {
    UploadRequestManager.shared.upload(String.self, request: MultipartRequest(), preferredLoadingView: true, retryAttempt: 3) { response in
      // MARK: - Handle your success response here
      print("Upload Success")
    } onFailure: { error in
      // MARK: - Handle your error here
      print("Upload Error")
    }

  }
}
