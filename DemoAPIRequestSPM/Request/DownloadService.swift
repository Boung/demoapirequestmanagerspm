//
//  DownloadService.swift
//  DemoSPMApplication
//
//  Created by Ly Boung on 22/8/23.
//

import Foundation
import APIRequestManagerSPM


class DownloadRequest: RequestProtocol {
  var baseURL: String { "https://img.freepik.com/premium-photo/beautiful-girl-hd-8k-wallpaper-stock-photographic-image_915071-13200.jpg" }
  
  var endPoint: String { "" }
  
  var httpHeader: APIRequestManagerSPM.HTTPHeader? { nil }
  
  var httpMethod: APIRequestManagerSPM.HTTPMethod { .get }
  
  var paramater: APIRequestManagerSPM.Paramater? { nil }
  
  var encoding: APIRequestManagerSPM.EncodingMethod { .jsonEncoding }
  
  var requiredAuthentication: Bool { false }
}

class DownloadService {
  static func download(completion: CallbackType<Data>?) {
    DownloadRequestManager.shared.downloadProgressHandler = { percentage in
      print("Downloaded Percentage: \(percentage)")
    }
    
    DownloadRequestManager.shared.download(request: DownloadRequest(), preferredLoadingView: true, retryAttempt: 3) { data in
      // MARK: - Handle your success response here
      print("Download Success")
    } onFailure: { error in
      // MARK: - Handle your error here
      print("Download Error")
    }

  }
}
