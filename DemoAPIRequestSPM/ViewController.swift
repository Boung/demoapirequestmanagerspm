//
//  ViewController.swift
//  DemoAPIRequestSPM
//
//  Created by Ly Boung on 7/9/23.
//

import UIKit

class ViewController: UIViewController {

  lazy var requestButton = UIButton()
  lazy var downloadButton = UIButton()
  lazy var uploadButton = UIButton()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    prepareLayouts()
    
    [requestButton, uploadButton, downloadButton].forEach {
      $0.addTarget(self, action: #selector(handleAction(_:)), for: .touchUpInside)
    }
  }
  
  @objc private func handleAction(_ button: UIButton) {
    let requestTag = 0
    let downloadTag = 1
    
    let buttonTag = button.tag
    
    switch buttonTag {
      case requestTag:
        APIService.requestAPI()
      case downloadTag:
        DownloadService.download { data in }
      default:
        MultipartService.upload()
    }
  }
  
  private func prepareLayouts() {
    view.backgroundColor = .white
    
    requestButton.tag = 0
    requestButton.setTitle("Request API", for: .normal)
    requestButton.setTitleColor(.white, for: .normal)
    requestButton.backgroundColor = .systemBlue
    
    downloadButton.tag = 1
    downloadButton.setTitle("Download API", for: .normal)
    downloadButton.setTitleColor(.white, for: .normal)
    downloadButton.backgroundColor = .systemBlue

    uploadButton.tag = 2
    uploadButton.setTitle("Upload API", for: .normal)
    uploadButton.setTitleColor(.white, for: .normal)
    uploadButton.backgroundColor = .systemBlue
    
    requestButton.translatesAutoresizingMaskIntoConstraints = false
    downloadButton.translatesAutoresizingMaskIntoConstraints = false
    uploadButton.translatesAutoresizingMaskIntoConstraints = false
    
    view.addSubview(requestButton)
    requestButton.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 50).isActive = true
    requestButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
    requestButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16).isActive = true
    
    view.addSubview(downloadButton)
    downloadButton.topAnchor.constraint(equalTo: requestButton.bottomAnchor, constant: 16).isActive = true
    downloadButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
    downloadButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16).isActive = true
    
    view.addSubview(uploadButton)
    uploadButton.topAnchor.constraint(equalTo: downloadButton.bottomAnchor, constant: 16).isActive = true
    uploadButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
    uploadButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16).isActive = true
  }
}

